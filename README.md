# GrimoireLab OrganizationsDB

One of optional GrimoireLab's configuration file is a `json` file that contains
information about companies and their associated email domains to be [imported by
GrimoireLab SortingHat](https://github.com/chaoss/grimoirelab-sortinghat#import--export)
during one of GrimoireLab deployment steps.

This repository hosts a master copy of such file to be used in my personal demos

# What's the format?

Basic template is:
```json
{
  "blacklist": [],
  "organizations": {
    ...
  },
  "time": "<DATE>",
  "uidentities": []
}
```

`DATE` notes the time and date of the last update of the file. For example:
`"time": "2019-16-09 07:00:00"`

The only importat object is `organizations`, that contains all the companies and
their domains information under the following format:
```json
"<COMPANY NAME>": [
    {
        "domain": "<DOMAIN>"
        "is_top": <BOOLEAN>
    },
    ...
]
```

I think `COMPANY NAME` and `DOMAIN` are obvious. `is_top` is used to note if the
domain companies' top domain. For example:
```json
"IBM": [
  {
    "domain": "au1.ibm.com",
    "is_top": false
  },
  {
    "domain": "br.ibm.com",
    "is_top": false
  },
  {
    "domain": "ca.ibm.com",
    "is_top": false
  },
  {
    "domain": "cn.ibm.com",
    "is_top": false
  },
  {
    "domain": "de.ibm.com",
    "is_top": false
  },
  {
    "domain": "ibm.com",
    "is_top": true
  },
  {
    "domain": "il.ibm.com",
    "is_top": false
  },
  {
    "domain": "in.ibm.com",
    "is_top": false
  },
  {
    "domain": "linux.vnet.ibm.com",
    "is_top": false
  },
  {
    "domain": "ozlabs.org",
    "is_top": true
  },
  {
    "domain": "platform.com",
    "is_top": true
  },
  {
    "domain": "us.ibm.com",
    "is_top": false
  }
],
```

# License

GPLv3